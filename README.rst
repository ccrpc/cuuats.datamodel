CUUATS Data Model Documentation
===============================

Introduction
------------

CUUATS Data Model is a lightweight data access layer for ArcGIS. It provides
tools to build Python classes that can be used to interact with data stored in
geodatabase feature classes. CUUATS Data Model was originally developed to
perform quality control on field data and to transform raw measurements into
indexed values using scales.

Compatibility
-------------

CUUATS Data Model is developed and tested with ArcGIS Desktop 10.2.2.

Credits
-------

CUUATS Data Model was developed by Matt Yoder for the Champaign Urbana
Urbanized Area Transportation Study (CUUATS). The many-to-many field was
developed by Edmond Lai.
